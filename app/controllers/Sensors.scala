package controllers

import java.util.Date
import javax.inject.Inject

import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json
import play.api.mvc._
import play.modules.reactivemongo.{MongoController, ReactiveMongoApi, ReactiveMongoComponents}
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson.{BSONDocument, BSONObjectID}
import repos.SensorRepoImpl

class Sensors @Inject()(val reactiveMongoApi: ReactiveMongoApi) extends Controller
    with MongoController with ReactiveMongoComponents {

  import controllers.SensorFields._

  def index = Action.async { implicit request =>
    SensorRepo.find().map(sensors => Ok(Json.toJson(sensors)))
  }

  def create = Action.async(BodyParsers.parse.json) { implicit request =>
    val sensor_id = (request.body \ SensorId).as[Int]
    val device_id = (request.body \ DeviceId).as[Int]
    val sensor_value = (request.body \ SensorValue).as[String]
    SensorRepo.save(BSONDocument(
      SensorId -> sensor_id,
      DeviceId -> device_id,
      SensorValue -> sensor_value,
      CreatedAt -> new Date().getTime
    )).map(result => Created)
  }

  def read(id: String) = Action.async { implicit request =>
    SensorRepo.select(BSONDocument(Id -> BSONObjectID(id))).map(sensor => Ok(Json.toJson(sensor)))
  }

  def update(id: String) = Action.async(BodyParsers.parse.json) { implicit request =>
    val sensor_id = (request.body \ SensorId).as[Int]
    val device_id = (request.body \ DeviceId).as[Int]
    val sensor_value = (request.body \ SensorValue).as[String]
    SensorRepo.update(BSONDocument(Id -> BSONObjectID(id)),
      BSONDocument("$set" -> BSONDocument(SensorId -> sensor_id, DeviceId -> device_id, SensorValue -> sensor_value)))
      .map(result => Accepted)
  }

  def delete(id: String) = Action.async {
    SensorRepo.remove(BSONDocument(Id -> BSONObjectID(id)))
      .map(result => Accepted)
  }

  def SensorRepo = new SensorRepoImpl(reactiveMongoApi)
}

object SensorFields {
  val Id = "_id"
  val SensorId = "sensor_id"
  val DeviceId = "device_id"
  val SensorValue = "sensor_value"
  val CreatedAt = "created_at"
}
